package com.example.fun;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class ConnectFour extends AppCompatActivity {

    /**
     * C11- C55: ImageButton variables to control the pressing of a cell.
     */
    private ImageButton C11;
    private ImageButton C12;
    private ImageButton C13;
    private ImageButton C14;
    private ImageButton C15;
    private ImageButton C21;
    private ImageButton C22;
    private ImageButton C23;
    private ImageButton C24;
    private ImageButton C25;
    private ImageButton C31;
    private ImageButton C32;
    private ImageButton C33;
    private ImageButton C34;
    private ImageButton C35;
    private ImageButton C41;
    private ImageButton C42;
    private ImageButton C43;
    private ImageButton C44;
    private ImageButton C45;
    private ImageButton C51;
    private ImageButton C52;
    private ImageButton C53;
    private ImageButton C54;
    private ImageButton C55;

    /**
     * Boolean variable to check the turn.
     */
    private boolean isPlayer1;

    /**
     * Textview to identify the current player.
     */
    private TextView turn;

    /**
     * A grid setup check the game status and decide winner.
     */
    private int[][] grid;
    /**
     * The current cell number which is made of two numbers, the row and column number.
     * e.g. cell # 35 is row 3 and column 5.
     */
    private int cellNum;
    /**
     * The current cell row number.
     */
    private int cellRow;
    /**
     * The current cell column number.
     */
    private int cellCol;
    /**
     * Counter to count the number of turn has been played. This is to check if the whole grid was filled, which becomes a draw.
     */
    private int counter;
    /**
     * Textview that shows the last winner.
     */
    private TextView lastWinner;

    /**
     * lastWinnerC4: SharePreference for the last score.
     * editor: an editor to lastWinner SharePreference.
     */
    SharedPreferences lastWinnerC4;
    SharedPreferences.Editor editor;

    private ImageView soundView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect_four);

        soundView = findViewById(R.id.sound_connectFour);


        if(MainActivity.isSoundOn){
            soundView.setImageDrawable(getDrawable(R.drawable.sound_speaker_on));
        }else{
            soundView.setImageDrawable(getDrawable(R.drawable.sound_speaker_off));
        }
        isPlayer1 = true;
        C11 = findViewById(R.id.C11);
        C12 = findViewById(R.id.C12);
        C13 = findViewById(R.id.C13);
        C14 = findViewById(R.id.C14);
        C15 = findViewById(R.id.C15);
        C21 = findViewById(R.id.C21);
        C22 = findViewById(R.id.C22);
        C23 = findViewById(R.id.C23);
        C24 = findViewById(R.id.C24);
        C25 = findViewById(R.id.C25);
        C31 = findViewById(R.id.C31);
        C32 = findViewById(R.id.C32);
        C33 = findViewById(R.id.C33);
        C34 = findViewById(R.id.C34);
        C35 = findViewById(R.id.C35);
        C41 = findViewById(R.id.C41);
        C42 = findViewById(R.id.C42);
        C43 = findViewById(R.id.C43);
        C44 = findViewById(R.id.C44);
        C45 = findViewById(R.id.C45);
        C51 = findViewById(R.id.C51);
        C52 = findViewById(R.id.C52);
        C53 = findViewById(R.id.C53);
        C54 = findViewById(R.id.C54);
        C55 = findViewById(R.id.C55);
        turn = findViewById(R.id.turn);
        counter = 0;
        cellNum = 0;
        cellRow = 0;
        cellCol = 0;
        grid = new int[5][5];

        lastWinner = findViewById(R.id.lastWon);
        lastWinnerC4 = getSharedPreferences("winner", Context.MODE_PRIVATE);
        editor = lastWinnerC4.edit();
        lastWinner.setText("Last Winner: \n" + lastWinnerC4.getString("winner", " ") + "");

        //Click Listeners for C11 - C55. Logic is make the coin falls to the lowest row possible.
        C11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(C11.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())){
                    if(C21.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())) {
                        C21.performClick();
                    }else {
                        start(C11);
                    }
                } }
        });
        C12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(C12.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())){
                    if(C22.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())) {
                        C22.performClick();
                    }else {
                        start(C12);
                    }
            } }
        });
        C13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)  {
                if(C13.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())){
                    if(C23.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())) {
                        C23.performClick();
                    }else {
                        start(C13);
                    }
                } }
        });
        C14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)  {
                if(C14.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())){
                    if(C24.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())) {
                        C24.performClick();
                    }else {
                        start(C14);
                    }
                } }
        });
        C15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)  {
                if(C15.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())){
                    if(C25.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())) {
                        C25.performClick();
                    }else {
                        start(C15);
                    }
                } }
        });
        C21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)  {
                if(C21.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())){
                    if(C31.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())) {
                        C31.performClick();
                    }else {
                        start(C21);
                    }
                } }
        });
        C22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)  {
                if(C22.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())){
                    if(C32.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())) {
                        C32.performClick();
                    }else {
                        start(C22);
                    }
                } }
        });
        C23.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)  {
                if(C23.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())){
                    if(C33.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())) {
                        C33.performClick();
                    }else {
                        start(C23);
                    }
                } }
        });
        C24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)  {
                if(C24.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())){
                    if(C34.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())) {
                        C34.performClick();
                    }else {
                        start(C24);
                    }
                } }
        });
        C25.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)  {
                if(C25.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())){
                    if(C35.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())) {
                        C35.performClick();
                    }else {
                        start(C25);
                    }
                } }
        });
        C31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)  {
                if(C31.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())){
                    if(C41.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())) {
                        C41.performClick();
                    }else {
                        start(C31);
                    }
                } }
        });
        C32.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)  {
                if(C32.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())){
                    if(C42.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())) {
                        C42.performClick();
                    }else {
                        start(C32);
                    }
                } }
        });
        C33.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)  {
                if(C33.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())){
                    if(C43.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())) {
                        C43.performClick();
                    }else {
                        start(C33);
                    }
                } }
        });
        C34.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)  {
                if(C34.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())){
                    if(C44.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())) {
                        C44.performClick();
                    }else {
                        start(C34);
                    }
                } }
        });
        C35.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)  {
                if(C35.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())){
                    if(C45.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())) {
                        C45.performClick();
                    }else {
                        start(C35);
                    }
                } }
        });
        C41.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)  {
                if(C41.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())){
                    if(C51.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())) {
                        C51.performClick();
                    }else {
                        start(C41);
                    }
                } }
        });
        C42.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)  {
                if(C42.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())){
                    if(C52.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())) {
                        C52.performClick();
                    }else {
                        start(C42);
                    }
                } }
        });
        C43.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)  {
                if(C43.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())){
                    if(C53.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())) {
                        C53.performClick();
                    }else {
                        start(C43);
                    }
                } }
        });
        C44.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)  {
                if(C44.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())){
                    if(C54.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())) {
                        C54.performClick();
                    }else {
                        start(C44);
                    }
                } }
        });
        C45.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)  {
                if(C45.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())){
                    if(C55.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())) {
                        C55.performClick();
                    }else {
                        start(C45);
                    }
                } }
        });
        C51.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)  {
                if(C51.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())){
                    start(C51);
                } }
        });
        C52.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)  {
                if(C52.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())){
                    start(C52);
                } }
        });
        C53.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)  {
                if(C53.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())){
                    start(C53);
                } }
        });
        C54.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)  {
                if(C54.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())){
                    start(C54);
                } }
        });
        C55.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)  {
                if(C55.getBackground().getConstantState().equals(getDrawable(android.R.drawable.presence_invisible).getConstantState())){
                    start(C55);
                } }
        });

    }

    /**
     * Starts the functionalities after a cell has been clicked.
     * @param cell
     */
    private void start(ImageButton cell){
        counter++;
        cellNum = Integer.parseInt(cell.getTag().toString());
        cellRow = (cellNum / 10)  - 1;
        cellCol = (cellNum % 10) - 1;
        if (isPlayer1){
            cell.setBackground(getDrawable(R.drawable.red));
            String temp = "Player 2 Turn";
            turn.setText(temp);
            turn.setTextColor(Color.parseColor("#FFD966"));
            grid[cellRow][cellCol] = 1;
        }
        else{
            cell.setBackground(getDrawable(R.drawable.yellow));
            String temp = "Player 1 Turn";
            turn.setText(temp);
            turn.setTextColor(Color.parseColor("#CD4248"));
            grid[cellRow][cellCol] = 2;
        }
        if(isPlayer1){
            if(areFourConnected(1)){
                //Player 1 has connected 4 in a row
                gameFinshed(1);
            }
        }
        else{
            if(areFourConnected(2)){
                //Player 2 has connected 4 in a row
                gameFinshed(2);
            }
        }
        if(counter == 25){
            //Grid was filled, the game is a draw.
            gameFinshed(0);
        }
        isPlayer1 = !isPlayer1;
    }

    /**
     * Helper method that checks if a player has connected 4 in a row
     * @param player number of the player. 1: Player 1. 2: Player 2.
     * @return true or false.
     */
    private boolean areFourConnected(int player){

        // horizontalCheck
        for (int j = 0; j< 5-3 ; j++ ){
            for (int i = 0; i<5; i++){
                if (grid[i][j] == player && grid[i][j+1] == player && grid[i][j+2] == player && grid[i][j+3] == player){
                    return true;
                }
            }
        }
        // verticalCheck
        for (int i = 0; i<5-3 ; i++ ){
            for (int j = 0; j<5; j++){
                if (grid[i][j] == player && grid[i+1][j] == player && grid[i+2][j] == player && grid[i+3][j] == player){
                    return true;
                }
            }
        }
        // ascendingDiagonalCheck
        for (int i=3; i<5; i++){
            for (int j=0; j<5-3; j++){
                if (grid[i][j] == player && grid[i-1][j+1] == player && grid[i-2][j+2] == player && grid[i-3][j+3] == player)
                    return true;
            }
        }
        // descendingDiagonalCheck
        for (int i=3; i<5; i++){
            for (int j=3; j<5; j++){
                if (grid[i][j] == player && grid[i-1][j-1] == player && grid[i-2][j-2] == player && grid[i-3][j-3] == player)
                    return true;
            }
        }
        return false;
    }

    /**
     * Private method that finishes the game.
     * @param status 0: game is a draw. 1: Player 1 has won. 2: Player 2 has won.
     */
    private void gameFinshed(int status){
        Popup popup;
        switch (status){
            case 0:
                editor.putString("winner", "It was a draw");
                editor.apply();
                popup = new Popup(ConnectFour.this, "Draw!", ConnectFour.this);
                popup.setCancelable(false);
                popup.show();
                break;
            case 1:
                editor.putString("winner", "Player 1");
                editor.apply();
                popup = new Popup(ConnectFour.this, "Player 1 Has Won!", ConnectFour.this);
                popup.setCancelable(false);
                popup.show();
                break;
            case 2:
                editor.putString("winner", "Player 2");
                editor.apply();
                popup = new Popup(ConnectFour.this, "Player 2 Has Won!", ConnectFour.this);
                popup.setCancelable(false);
                popup.show();
        }

    }

    /**
     * Restart the game and resets everything.
     */
    public void rematch(){
        Intent intent = new Intent(this, ConnectFour.class);
        startActivity(intent);
        finish();
    }

    /**
     * Takes the user the homepage.
     */
    public void home(){
        finish();
    }

    /**
     * BackGround Music Controller
     * @param view
     */
    public void onClickMusic(View view){
        MainActivity.isSoundOn= !MainActivity.isSoundOn;
        if(MainActivity.isSoundOn){
            soundView.setImageDrawable(getDrawable(R.drawable.sound_speaker_on));
            MainActivity.md.start();
        }else{
            soundView.setImageDrawable(getDrawable(R.drawable.sound_speaker_off));
            MainActivity.md.pause();
        }
    }
}