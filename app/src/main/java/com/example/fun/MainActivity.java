package com.example.fun;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    /**
     * A button that takes you to the account page.
     */
    private Button game;
    private Button game2;
    private Button game3;

    private ImageView speakerBtn;
    /**
     * True if sound is on; false otherwise
     */
    public static boolean isSoundOn;

    /**
     * Media player for the whacking sound.
     */
    public static MediaPlayer md;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isSoundOn = true;
        setContentView(R.layout.activity_main);

        speakerBtn = findViewById(R.id.speakerBtn);

        game = findViewById(R.id.connectGame_btn);
        game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ConnectFour.class);
                startActivity(intent);
            }
        });

        game2 = findViewById(R.id.seeYouThereGame_btn);
        game2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SeeYouThere.class);
                startActivity(intent);
            }
        });
        game3 = findViewById(R.id.ticTacToeGame_btn);
        game3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, TicTacToe.class);
                startActivity(intent);
            }
        });

        md = MediaPlayer.create(this, R.raw.main_page_sound);
        md.start();
        runAnimation();
        speakerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.isSoundOn= !MainActivity.isSoundOn;
                if(MainActivity.isSoundOn){
                    speakerBtn.setImageDrawable(getDrawable(R.drawable.soundon));
                    MainActivity.md.start();
                }else{
                    speakerBtn.setImageDrawable(getDrawable(R.drawable.soundoff));
                    MainActivity.md.pause();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(MainActivity.isSoundOn){
            speakerBtn.setImageDrawable(getDrawable(R.drawable.soundon));
        }else{
            speakerBtn.setImageDrawable(getDrawable(R.drawable.soundoff));
        }
    }

    private void runAnimation() {

        RotateAnimation anim = new RotateAnimation(0f, 360f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(5000);
        TextView tv = findViewById(R.id.app_name);
        tv.startAnimation(anim);

    }

}