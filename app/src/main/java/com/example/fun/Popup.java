package com.example.fun;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;

public class Popup extends Dialog {

    private final String message;
    private int currentGame;

    private final int CONNECT_FOUR = 0;
    private final int SEE_YOU_THERE = 1;

    private final int TIC_TAC_TOE = 2;
    private ConnectFour connectFour;
    private SeeYouThere seeGame;

    private TicTacToe ticTacToe;

    public Popup(@NonNull Context context, String message, ConnectFour connectFour) {
        super(context);
        this.message = message;
        this.connectFour = connectFour;
        currentGame = CONNECT_FOUR;
    }
    public Popup(@NonNull Context context, String message, SeeYouThere seeYouThere) {
        super(context);
        this.message = message;
        this.seeGame = seeYouThere;
        currentGame = SEE_YOU_THERE;
    }

    public Popup(@NonNull Context context, String message, TicTacToe ticTacToe) {
        super(context);
        this.message = message;
        this.ticTacToe = ticTacToe;
        currentGame = TIC_TAC_TOE;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.popup_layout);
        final TextView messagetxt = findViewById(R.id.message);
        final Button rematch = findViewById(R.id.rematch);
        final Button home = findViewById(R.id.home);


        messagetxt.setText(message);

        rematch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if(currentGame==CONNECT_FOUR) {
                    connectFour.rematch();
                }else if(currentGame==SEE_YOU_THERE){
                    seeGame.rematch();
                }
                else if(currentGame==TIC_TAC_TOE){
                    ticTacToe.rematch();
                }
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentGame==SEE_YOU_THERE) {
                    seeGame.home();
                } else if (currentGame==CONNECT_FOUR) {
                    connectFour.home();
                }
                else if(currentGame==TIC_TAC_TOE){
                    ticTacToe.home();
                }
            }
        });
    }


}
