package com.example.fun;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Random;

public class SeeYouThere extends AppCompatActivity {
    /**
     * ImageButton of the dice
     */
    private ImageButton dice;
    /**
     * Player 1 score
     */
    private int player1Score=1;
    /**
     * Player 2 score
     */
    private int player2Score=1;
    /**
     * Tracks current player:: 1 for player 1 and 2 for player 2
     */
    private int currentPlayer=1;
    /**
     * Dice number after rolling
     */
    private int diceNum=1;
    /**
     * Player 1 location
     */
    private ImageView player1Location;
    /**
     * Player 2 location
     */
    private ImageView player2Location;
    /**
     * List of all locations
     */
    private ArrayList<ImageView> locations;
    /**
     * Shows whose turn it is
     */
    private TextView playerTurn;
    /**
     * Shows the previous winner
     *
     */
    private TextView previousWinnerText;
    /**
     * Popup menu after the game ends
     */
    private  Popup popup;

    /**
     * Shows previous winner.
     * 0 if no previous winner yet, 1 if player 1 won, 2 if player 2 won
     */
    private int previousWinner=0;

    /**
     * Game handler
     */
    Handler handler = new Handler();
    private ImageView soundView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.seeyouthere);

        soundView=findViewById(R.id.sound_tictactoe);

        if(MainActivity.isSoundOn){
            soundView.setImageDrawable(getDrawable(R.drawable.sound_speaker_on));
        }else{
            soundView.setImageDrawable(getDrawable(R.drawable.sound_speaker_off));
        }

        dice = findViewById(R.id.dice);
        dice.setImageResource(R.drawable.dice1);
        locations = new ArrayList<>();
        locations.add(findViewById(R.id.s1));
        locations.add(findViewById(R.id.s2));
        locations.add(findViewById(R.id.s3));
        locations.add(findViewById(R.id.s4));
        locations.add(findViewById(R.id.trap5));
        locations.add(findViewById(R.id.s6));
        locations.add(findViewById(R.id.s7));
        locations.add(findViewById(R.id.s8));
        locations.add(findViewById(R.id.s9));
        locations.add(findViewById(R.id.s10));
        locations.add(findViewById(R.id.s11));
        locations.add(findViewById(R.id.s12));
        locations.add(findViewById(R.id.s13));
        locations.add(findViewById(R.id.s14));
        locations.add(findViewById(R.id.s15));
        locations.add(findViewById(R.id.s16));
        locations.add(findViewById(R.id.s17));
        locations.add(findViewById(R.id.trap18));
        locations.add(findViewById(R.id.s19));
        locations.add(findViewById(R.id.s20));
        locations.add(findViewById(R.id.s21));
        locations.add(findViewById(R.id.s22));
        locations.add(findViewById(R.id.s23));
        locations.add(findViewById(R.id.s24));
        locations.add(findViewById(R.id.s25));
        locations.add(findViewById(R.id.s26));
        locations.add(findViewById(R.id.s27));
        locations.add(findViewById(R.id.s28));
        locations.add(findViewById(R.id.s29));
        locations.add(findViewById(R.id.s30));
        locations.add(findViewById(R.id.trap31));
        locations.add(findViewById(R.id.s32));


        player2Location= locations.get(0);
        player2Location.setForeground(getDrawable(R.drawable.player2));
        player1Location= locations.get(0);
        player1Location.setForeground(getDrawable(R.drawable.player1));

        playerTurn= findViewById(R.id.playerTurn);
        playerTurn.setText("Player 1's Turn");
        previousWinnerText= findViewById(R.id.previousWinner);
   }

   /**
   Resets game
    */
   public void rematch(){

       player2Location.setForeground(null);
       player1Location.setForeground(null);

       player2Location= locations.get(0);
       player2Location.setForeground(getDrawable(R.drawable.player2));
       player1Location= locations.get(0);
       player1Location.setForeground(getDrawable(R.drawable.player1));
       player1Score=1;
       player2Score=1;
       currentPlayer=1;
       diceNum=0;
       updateScores();
       if(currentPlayer==1){

           player1Location= locations.get(player1Score-1);
           player2Location.setForeground(getDrawable(R.drawable.player2));
           player1Location.setForeground(getDrawable(R.drawable.player1));
           currentPlayer=2;
           playerTurn.setText("PLAYER 2's TURN");
       }else{

           player2Location= locations.get(player2Score-1);
           player1Location.setForeground(getDrawable(R.drawable.player1));
           player2Location.setForeground(getDrawable(R.drawable.player2));
           currentPlayer=1;
           playerTurn.setText("PLAYER 1's TURN");
       }
       if(previousWinner==1){
           previousWinnerText.setText("Previous Winner: Player 1");

       } else if (previousWinner==2) {
           previousWinnerText.setText("Previous Winner: Player 2");
       }else{
           previousWinnerText.setText(" ");
       }

       popup.dismiss();
   }
    /**
        Rolls the Dice
     */
   public void onClickDice(View view){


       Random r= new Random();
       int num= r.nextInt(6+1);
       handler.postDelayed(new Runnable() {

           public void run() {

               int num= r.nextInt(6+1);

               if(num==1){
                   dice.setImageResource(R.drawable.dice1);
                   diceNum=1;
               }
               if(num==2){
                   dice.setImageResource(R.drawable.dice2);
                   diceNum=2;
               }
               if(num==3){
                   dice.setImageResource(R.drawable.dice3);
                   diceNum=3;
               }
               if(num==4){
                   dice.setImageResource(R.drawable.dice4);
                   diceNum=4;
               } if(num==5){
                   dice.setImageResource(R.drawable.dice5);
                   diceNum=5;
               }
               if(num==6){
                   dice.setImageResource(R.drawable.dice6);
                   diceNum=6;
               }
               if(r.nextInt(10)!=8) {
                   handler.postDelayed(this, 100);
               }else{
                   handler.postDelayed( updateScores(), 1000);

               }
           }

       }, 100);
   }

    /**
     Updates the scores after each turn
     */
    public Runnable updateScores(){

        if(currentPlayer==1){
            player1Score = player1Score+diceNum;
            if(player1Score == 5 || player1Score==18 || player1Score==31){
                player1Score = 1;
            }
            if(player1Score>32){
                player1Score = player1Score-diceNum;
            }
            if(player1Score==player2Score){
                player2Score=1;
            }
        }else{
            player2Score = player2Score+diceNum;
            if(player2Score == 5 || player2Score==18 || player2Score==31){
                player2Score = 1;
            }
            if(player2Score>32){
                player2Score = player2Score-diceNum;
            }
            if(player2Score==player1Score){
                player1Score=1;
            }
        }

        handler.postDelayed( displayUpdate(), 2000);
        isEnd();

        return null;
    }

    /**
     Checks if a player wins
     */
    private void isEnd() {

        if(player1Score == 32){
            playerTurn.setText("Player 1 Wins!!");
            previousWinner=1;


            popup = new Popup(SeeYouThere.this, "Player 1 Has Won!", SeeYouThere.this);
            popup.setCancelable(false);
            popup.show();


            }
        else if(player2Score == 32){
            playerTurn.setText("Player 2 Wins!!");
            previousWinner=2;

              popup = new Popup(SeeYouThere.this, "Player 2 Has Won!", SeeYouThere.this);
             popup.setCancelable(false);
             popup.show();

        }


    }

    /**
     Displays the updated game
     */

    public Runnable displayUpdate(){

        player1Location.setForeground(null);
        player2Location.setForeground(null);
        player1Location= locations.get(player1Score-1);
        player2Location= locations.get(player2Score-1);

        if(currentPlayer==1){

            player1Location= locations.get(player1Score-1);
            player2Location.setForeground(getDrawable(R.drawable.player2));
            player1Location.setForeground(getDrawable(R.drawable.player1));
            currentPlayer=2;
            playerTurn.setText("PLAYER 2's TURN");
        }else{

            player2Location= locations.get(player2Score-1);
            player1Location.setForeground(getDrawable(R.drawable.player1));
            player2Location.setForeground(getDrawable(R.drawable.player2));
            currentPlayer=1;
            playerTurn.setText("PLAYER 1's TURN");
        }
        if(previousWinner==1){
            previousWinnerText.setText("Previous Winner: Player 1");
            
        } else if (previousWinner==2) {
            previousWinnerText.setText("Previous Winner: Player 2");
        }else{
            previousWinnerText.setText(" ");
        }

        return null;

    }

    /**
     * BackGround Music Controller
     * @param view
     */
    public void onClickMusic(View view){
        MainActivity.isSoundOn= !MainActivity.isSoundOn;
        if(MainActivity.isSoundOn){
            soundView.setImageDrawable(getDrawable(R.drawable.sound_speaker_on));
            MainActivity.md.start();
        }else{
            soundView.setImageDrawable(getDrawable(R.drawable.sound_speaker_off));
            MainActivity.md.pause();
        }
    }

    /**
     * Returns to home Page
     */
    public void home(){
        finish();
    }
}
