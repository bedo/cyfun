package com.example.fun;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.fun.R;

import java.util.ArrayList;
import java.util.List;

public class TicTacToe extends AppCompatActivity {
    /**
     * top row, leftmost corner square
     */
    private ImageView square1;
    /**
     * top row, middle square
     */
    private ImageView square2;
    /**
     * top row, rightmost square
     */
    private ImageView square3;
    /**
     * middle row, leftmost square
     */
    private ImageView square4;
    /**
     * middle row, middle square
     */
    private ImageView square5;
    /**
     * middle row, rightmost square
     */
    private ImageView square6;
    /**
     * bottom row, leftmost square
     */
    private ImageView square7;
    /**
     * bottom row, middle square
     */
    private ImageView square8;
    /**
     * bottom row, rightmost square
     */
    private ImageView square9;
    /**
     * List of winning combinations
     */
    private List<int[]> winList = new ArrayList<>();
    /**
     * Array to track the state of each square (0: Empty, 1: Player 1, 2: Player 2)
     */
    private int [] pos = {0,0,0,0,0,0,0,0,0};
    /**
     * Current player's turn (1 or 2)
     */
    private int turn = 1;
    /**
     * Number of occupied squares
     */
    private int  occupiedSquares = 1;
    /**
     * Dialog for rematch
     */
    private Popup popup;

    private TextView lastWinner;

    /**
     * lastWinnerC4: SharedPrefrence for the last score.
     */
    SharedPreferences lastWinnerTTT;
    SharedPreferences.Editor editor;

    private ImageView soundView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tic_tac_toe);

        soundView=findViewById(R.id.sound_tictactoe);

        if(MainActivity.isSoundOn){
            soundView.setImageDrawable(getDrawable(R.drawable.sound_speaker_on));
        }else{
            soundView.setImageDrawable(getDrawable(R.drawable.sound_speaker_off));
        }

        square1 = findViewById(R.id.square1);
        square2 = findViewById(R.id.square2);
        square3 = findViewById(R.id.square3);
        square4 = findViewById(R.id.square4);
        square5 = findViewById(R.id.square5);
        square6 = findViewById(R.id.square6);
        square7 = findViewById(R.id.square7);
        square8 = findViewById(R.id.square8);
        square9 = findViewById(R.id.square9);


        winList.add(new int[]{0,1,2});
        winList.add(new int[]{0,3,6});
        winList.add(new int[]{0,4,8});
        winList.add(new int[]{1,4,7});
        winList.add(new int[]{2,5,8});
        winList.add(new int[]{2,4,6});
        winList.add(new int[]{3,4,5});
        winList.add(new int[]{6,7,8});

        lastWinner = findViewById(R.id.lastWon2);
        lastWinnerTTT = getSharedPreferences("winner2", Context.MODE_PRIVATE);
        editor = lastWinnerTTT.edit();
        lastWinner.setText("Last Winner: \n" + lastWinnerTTT.getString("winner2", " ") + "");

        square1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isEmpty(0)){
                    makeAMove(0,(ImageView)view);
                }
            }
        });

        square2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isEmpty(1)){
                    makeAMove(1,(ImageView)view);
                }
            }
        });

        square3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isEmpty(2)){
                    makeAMove(2,(ImageView)view);
                }
            }
        });

        square4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isEmpty(3)){
                    makeAMove(3,(ImageView)view);
                }
            }
        });

        square5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isEmpty(4)){
                    makeAMove(4,(ImageView)view);
                }
            }
        });

        square6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isEmpty(5)){
                    makeAMove(5,(ImageView)view);
                }
            }
        });

        square7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isEmpty(6)){
                    makeAMove(6,(ImageView)view);
                }
            }
        });

        square8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isEmpty(7)){
                    makeAMove(7,(ImageView)view);
                }
            }
        });

        square9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isEmpty(8)){
                    makeAMove(8,(ImageView)view);
                }
            }
        });
    }

    /**
     * Checks if a given square is empty.
     *
     * @param squareNum The index of the square (0-8).
     * @return `true` if the square is empty, `false` otherwise.
     */
    private boolean isEmpty(int squareNum){
        boolean flag = false;
        if(pos[squareNum] == 0){
            flag = true;
        }
        return flag;
    }

    /**
     * Handles the logic when a player makes a move on a square.
     *
     * @param squareSelected The index of the selected square (0-8).
     * @param imageView The ImageView representing the selected square.
     */
    private void makeAMove(int squareSelected, ImageView imageView){
        pos[squareSelected] = turn;
        if(turn == 1){
            imageView.setImageResource(R.drawable.ximage);
            if(win()){
                editor.putString("winner2", "Player one");
                editor.apply();
                popup = new Popup(TicTacToe.this,"Player one won the round", TicTacToe.this );
                popup.setCancelable(false);
                popup.show();
            }
            else if(occupiedSquares == 9){
                editor.putString("winner2", "It was a draw");
                editor.apply();
                popup = new Popup(TicTacToe.this,"Draw!!", TicTacToe.this );
                popup.setCancelable(false);
                popup.show();
            }
            else{
                turn = 2;
                occupiedSquares++;

            }
        }
        else{
            imageView.setImageResource(R.drawable.oimage);
            if(win()){
                editor.putString("winner2", "Player two");
                editor.apply();
                popup = new Popup(TicTacToe.this,"Player two won the round", TicTacToe.this );
                popup.setCancelable(false);
                popup.show();
            }
            else if(occupiedSquares == 9){
                editor.putString("winner2", "It was a draw");
                editor.apply();
                popup = new Popup(TicTacToe.this,"Draw!!", TicTacToe.this );
                popup.setCancelable(false);
                popup.show();
            }
            else{
                turn = 1;
                occupiedSquares++;
            }

        }
    }

    /**
     * Checks for a win in any of the winning combinations.
     *
     * @return `true` if there is a win, `false` otherwise.
     */
    private boolean win(){
        boolean win = false;
        for(int i = 0; i < winList.size(); i++){
            int [] winSeries = winList.get(i);
            if(pos[winSeries[0]] == turn && pos[winSeries[1]] == turn && pos[winSeries[2]] == turn){
                win = true;
            }
        }
        return win;
    }


    /**
     * Resets the game state for a new round.
     */
    public void rematch(){
        Intent intent = new Intent(this, TicTacToe.class);
        startActivity(intent);
        finish();
    }

    public void home(){
        finish();
    }

    /**
     * BackGround Music Controller
     * @param view
     */
    public void onClickMusic(View view){
        MainActivity.isSoundOn= !MainActivity.isSoundOn;
        if(MainActivity.isSoundOn){
            soundView.setImageDrawable(getDrawable(R.drawable.sound_speaker_on));
            MainActivity.md.start();
        }else{
            soundView.setImageDrawable(getDrawable(R.drawable.sound_speaker_off));
            MainActivity.md.pause();
        }
    }
}